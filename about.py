
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from settings import ABOUT_TEXT
import subprocess
import platform
import core
import telegram

PLUGINVERSION = 2
plugin = core.Plugin()
ABOUT_STRING = "OctoBot based on commit <code>" + subprocess.check_output('git log -n 1 --pretty="%h"', shell=True).decode('utf-8') + "</code>"
ABOUT_STRING += "Branch: <code>" +  subprocess.check_output('git rev-parse --abbrev-ref HEAD', shell=True).decode('utf-8') + "</code>"
ABOUT_STRING += subprocess.check_output('git --git-dir core/.git log -n 1 --pretty=" OctoBot-Core based on commit <code>%h</code>"', shell=True).decode('utf-8')[1:]
ABOUT_STRING += subprocess.check_output('git --git-dir plugins/.git log -n 1 --pretty=" OctoBot-Plugins based on commit <code>%h</code>"', shell=True).decode('utf-8')[1:]
ABOUT_STRING += "Python-Telegram-Bot version: <code>" + telegram.__version__ + "</code>\n"
ABOUT_STRING += "Running on <code>" + platform.platform() + "</code>." + "\nPython: <code>" + platform.python_implementation() + " " + platform.python_version() + "</code>\n"
ABOUT_STRING += '<a href="gitlab.com/aigis_bot">GitLab page</a>\n'
ABOUT_STRING += ABOUT_TEXT

@plugin.command(command="/about",
                description="About bot",
                inline_supported=True,
                hidden=False)
def about(bot, update, user, args):
    return core.message(text=ABOUT_STRING, parse_mode="HTML")
