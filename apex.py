import core
import re
import requests
import settings
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin("Apex stats")
ENDPOINT = "http://api.mozambiquehe.re/bridge"

def good_username(strg, search=re.compile(r'[^a-zA-Z\-_0-9 ]').search):
    return not bool(search(strg))

def get_apex_stats(platform, username):
    if good_username(username):
        r = requests.get(ENDPOINT, params={"platform":platform, "player":username, "auth":settings.MOZAMBIQUE_HERE_API_KEY, "version":2})
        if r.status_code == 404:
            return core.message("Player not found", failed=True)
        elif r.status_code == 200:
            player_data = r.json()
            stats = []
            stats.append(f'👤Player {player_data["global"]["name"]} '
                         f'on {player_data["global"]["platform"]}')
            stats.append(f'Level: {player_data["global"]["level"]}')
            stats.append(f'Ranked level: {player_data["global"]["rank"]["rankName"]}'
                         f', {player_data["global"]["rank"]["rankDiv"]}')
            if int(player_data["global"]["battlepass"]["level"]) > 0:
                stats.append(f'Battlepass level: {player_data["global"]["battlepass"]["level"]}')
            if player_data["realtime"]["isInGame"] == '1':
                stats.append(f'Playing right now')
            elif player_data["realtime"]["isOnline"] == '1':
                stats.append(f'In lobby')
            if player_data["realtime"]["isInGame"] == '1' or player_data["realtime"]["isInGame"] == '1':
                stats.append(f'- Lobby is {player_data["realtime"]["lobbyState"]}')
                if player_data["realtime"]["canJoin"] == '1':
                    stats.append(f'- You can join')
            stats.append(f"Plays as {player_data['realtime']['selectedLegend']}:")
            legend = player_data["legends"]["all"][player_data['realtime']['selectedLegend']]
            for stat in legend["data"].values():
                stats.append(f'- {stat["name"]}: {stat["value"]}')
            print(legend["ImgAssets"]["icon"])
            return core.message("\n".join(stats), photo=legend["ImgAssets"]["icon"])
        else:
            return core.message(f"Mozambiquehe.re returned status code {r.status_code}", failed=True)
    else:
        return core.message("Invalid username passed", failed=True)


@plugin.command(command="/apex_pc",
                description="Apex legends stats on PC",
                inline_supported=True,
                hidden=False,
                required_args=1)
def apex_pc(bot, update, user, args):
    return get_apex_stats('PC', args[0]) # Origin usernames can have only one word


@plugin.command(command="/apex_ps4",
                description="Apex legends stats on PS4",
                inline_supported=True,
                hidden=False,
                required_args=1)
def apex_ps4(bot, update, user, args):
    return get_apex_stats("PS4", " ".join(args))

@plugin.command(command="/apex_x1",
                description="Apex legends stats on X1",
                inline_supported=True,
                hidden=False,
                required_args=1)
def apex_x1(bot, update, user, args):
    return get_apex_stats("X1", " ".join(args))

@plugin.command(command="/username_test",
                description="test username",
                inline_supported=True,
                hidden=True,
                required_args=1)
def apex_username(bot, update, user, args):
    return str(good_username(" ".join(args)))
