
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import bitmath
import core
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
MSGTMPL = """
%(val_a)s = %(val_b)s
"""
FORMATTER = "{value:.4f} {unit}"
@plugin.command(command="/byte",
                description="Converts memory sizes",
                inline_supported=True,
                hidden=False)
def convert(bot, update, user, args):
    if len(args) > 0:
        try:
            if "to" in args:
                args = "".join(args).split("to")
                print(args)
                d = {
                "val_a": bitmath.parse_string_unsafe(args[0]),
                }
                d["val_b"] = bitmath.parse_string_unsafe("1" + args[1]).from_other(d["val_a"])
            else:
                va = bitmath.parse_string_unsafe(" ".join(args))
                d = {
                    "val_a": va,
                    "val_b":va.best_prefix()
                }
            for value in d:
                d[value] = d[value].format(FORMATTER)
            return core.message(MSGTMPL % d)        
        except ValueError as e:
                return core.message(str(e).title())
    else:
        return core.message("No data supplied!")
