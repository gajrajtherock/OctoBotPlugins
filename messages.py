
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import random
import telegram
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
@plugin.command(command="/first_message",
                description="Replies to first message in chat",
                inline_supported=False,
                hidden=False)
def fmsg(bot, update, user, args):
    try:
        bot.sendMessage(update.message.chat.id, "Here is first message in this chat", reply_to_message_id=1)
    except telegram.error.BadRequest:
        return core.message('Seems like first message of this chat was deleted.', failed=True)

@plugin.command(command="/random_message",
                description="Replies to random message in chat",
                inline_supported=False,
                hidden=False)
def rmsg(bot, update, user, args):
    try:
        bot.sendMessage(update.message.chat.id, "Random message", reply_to_message_id=random.randint(1, update.message.message_id))
    except telegram.error.BadRequest:
        return core.message("I tried to reply to random message, but it seems to be deleted! Try using command again", failed=True)
