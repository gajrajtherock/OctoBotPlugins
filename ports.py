
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import requests
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
PORTINFO = """
Port <code>#%(port)s</code>
<b>Description</b>: <i>%(description)s</i>
<b>Status</b>: <i>%(status)s</i>
<b>TCP</b>:<i>%(tcp)s</i>
<b>UDP</b>:<i>%(udp)s</i>
"""
PORTLIST = requests.get(
    "https://raw.githubusercontent.com/mephux/ports.json/master/ports.lists.json").json()


@plugin.command(command="/port",
                description="Sends information about port",
                inline_supported=True,
                required_args=1,
                hidden=False)
def port(bot, update, user, args):
    if " ".join(args) in PORTLIST:
        ports = []
        for port in PORTLIST[" ".join(args)]:
            port["description"] = port["description"].replace("â€”", " ")
            ports.append(PORTINFO % port)
        return core.message(text="\nAlso may be:".join(ports), parse_mode="HTML")
    else:
        return core.message(text="No such port in database", failed=True)
