
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import html

import textwrap
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import settings
import logging
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
LOGGER = logging.getLogger(__file__)

CORELOCALES = core._localization.get_locales_dict("core")

@plugin.command(command="/start",
                inline_supported=False,
                hidden=True)
def start(bot, update, user, args):
    def _(x): return core.locale.get_localized(x, update.message.chat.id)
    if len(args) > 0:
        update.message.text = "/" + " ".join(update.message.text.split(" ")[1:])
        LOGGER.debug("Forwarding command to %s", update.message.text)
        func = bot.modloader.handle_command(update)
        if func is not None:
            return func(bot, update, user, args)
        else:
            return core.message("Command not found")
    kbd = [
            [InlineKeyboardButton(
                text=_(CORELOCALES["help_button"]), url=settings.WEBSITE + "#cmdlist")],
            [InlineKeyboardButton(
                text=_(CORELOCALES["news_button"]), url=settings.NEWS_LINK)],
            [InlineKeyboardButton(
                text=_(CORELOCALES["chat_button"]), url=settings.CHAT_LINK)],
        ]
    for extra_link in settings.EXTRA_LINKS:
        name, link = list(extra_link.items())[0]
        kbd.append([InlineKeyboardButton(text=name, url=link)])
    return core.message(_(CORELOCALES["start"]) % bot.getMe().first_name, inline_keyboard=InlineKeyboardMarkup(kbd))



@plugin.update()
def new_member(bot, update):
    if update.message:
        if update.message.new_chat_members:
            for member in update.message.new_chat_members:
                if member.id == bot.getMe().id:
                    def _(x): return core.locale.get_localized(x, update.message.chat.id)
                    kbd = InlineKeyboardMarkup(
                        [
                            [InlineKeyboardButton(
                                text=_(CORELOCALES["help_button"]), url=settings.WEBSITE + "#cmdlist")],
                            [InlineKeyboardButton(
                                text=_(CORELOCALES["news_button"]), url=settings.NEWS_LINK)],
                            [InlineKeyboardButton(
                                text=_(CORELOCALES["chat_button"]), url=settings.CHAT_LINK)],
                        ]
                    )
                    return core.message(_(CORELOCALES["start"]) % bot.getMe().first_name, inline_keyboard=kbd)


@plugin.command(command="/help", hidden=True, inline_supported=False)
def coreplug_help(bot, update, user, args):
    def _(x): return core.locale.get_localized(x, update.message.chat.id)
    if args:
        LOGGER.debug(args)
        for plugin in bot.modloader.plugins:
            for command_ in plugin.commands:
                cmd_alias = command_.command
                if isinstance(cmd_alias, str):
                    cmd_alias = [cmd_alias]
                for command in cmd_alias:
                    LOGGER.debug(command)
                    if "/" + args[0].lower() == command.lower():
                        info = {"command": "/" + args[0], "description": command_.description,
                                "docs": _(bot.modloader.locales["not_available"])}
                        if command_.execute.__doc__:
                            info["docs"] = html.escape(
                                textwrap.dedent(command_.execute.__doc__))
                        return core.message(_(bot.modloader.locales["help_format"]) % info, parse_mode="HTML")
        return core.locale.get_localized(bot.modloader.locales["unknown_help_command"], update.message.chat.id)
    else:
        if update.message.chat.type == "private":
            return core.message(bot.modloader.gen_help(update.message.chat.id), parse_mode="HTML")
        else:
            keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(
                text=_(bot.modloader.locales["help_button_into_pm"]), url="http://t.me/%s?start=help" % bot.getMe().username)]])
            return core.message(_(bot.modloader.locales["help_button_into_pm_text"]), inline_keyboard=keyboard)
