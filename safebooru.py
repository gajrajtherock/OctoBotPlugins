
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from telegram_ui import catalog_provider
from requests import get
import bs4
import html
import core
import re
import logging


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


message = """
<b>Rating:</b> <i>%(rating)s</i>
<b>Tags:</b> <i>%(tags)s</i>
<a href="https://safebooru.org/index.php?page=post&s=view&id=%(id)s">Image on safebooru</a>
"""
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="safebooru")
HEADERS = {"User-Agent": "OctoBot/1.0"}
LOGGER = logging.getLogger("safebooru")


def safebooru_search(term, number, count=1):
    image = get("https://safebooru.org/index.php",
                     params={
                        "page":"dapi",
                        "s":"post",
                        "q":"index",
                        "tags":term,
                        "limit":count,
                        "pid":number-1
                     },
                     headers=HEADERS)
    api_q = bs4.BeautifulSoup(image.text, "html.parser").posts
    if int(api_q.attrs["count"]) > 0:
        resp = []
        for post in api_q.find_all("post"):
            post.attrs["tags"] = html.escape(post.attrs["tags"][:1024])
            resp.append(catalog_provider.CatalogKey(text=message % post.attrs, 
                                               image=post.attrs["file_url"],
                                               thumbnail=post.attrs["preview_url"]))
        return resp, api_q.attrs["count"]
    else:
        raise IndexError("Not found")

catalog_provider.create_catalog(plugin, safebooru_search, ["/sb", "/safebooru"], "Searches for query on safebooru")
