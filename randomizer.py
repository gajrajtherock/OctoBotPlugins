
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import random
import core
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="Randomizer")


@plugin.command(command="/random",
                description="Get random number between a and b")
def random_between(bot, update, user, args):
    if len(args) < 2:
        return core.message(
            "You should provide minimum and maximum. Example: /random 3 20",
            failed=True
        )
    try:
        a, b = int(args[0]), int(args[1])
        if a == b:
            return core.message("Minimum equals maximum!", failed=True)
        random.seed(update.message.date)
        num = random.randint(a, b)
        return core.message("🎲 {} is your number.".format(num))
    except ValueError:
        return core.message("You supplied invalid numbers!", failed=True)


@plugin.command(command="/flipcoin",
                description="Flip a coin (get random number between 0 and 1)")
def flip_coin(bot, update, user, args):
    random.seed(update.message.date)
    num = random.randint(0, 1)
    return core.message("👉 {}!".format("Heads" if num else "Tails"))
