
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""Colors module"""
from io import BytesIO

from PIL import Image, ImageColor
from telegram import Bot, Update
import core
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="Color Sampler")


@plugin.command(command="/color",
                description="Create color samples",
                inline_supported=True,
                required_args=1,
                hidden=False)
def rgb(b: Bot, u: Update, user, args):
    """
    Create color samples.
    Supports both HEX and RGB
    Example:
    User:
    /color #FF0000

    OctoBot:
    [ Photo ]

    OctoBot:
    #FF0000

    User:
    /color 255 0 0

    OctoBot:
    [ Photo ]

    OctoBot Dev:
    [255, 0, 0]

    User:
    /color 0xFF 0x0 0x0

    OctoBot:
    [ Photo ]

    OctoBot:
    [255, 0, 0]
    """
    try:
        if args[0].startswith("#"):
            color = args[0]
            try:
                usercolor = ImageColor.getrgb(color)
            except Exception:
                return core.message("Invalid Color Code supplied", failed=True)
        elif args[0].startswith("0x"):
            if len(args) > 2:
                    usercolor = int(args[0][2:], 16), int(
                        args[1][2:], 16), int(args[2][2:], 16)
            else:
                color = "#"+args[0][2:]
                usercolor = ImageColor.getrgb(color)
        else:
            usercolor = int(args[0]), int(args[1]), int(args[2])
    except ValueError:
        return core.message("Invalid HEX color/RGB colors provided.\n(spoiler: /color red and other will NOT work)",
                            failed=True)
    color = usercolor
    im = Image.new(mode="RGB", size=(128, 128), color=usercolor)
    file = BytesIO()
    im.save(file, "PNG")
    file.seek(0)
    return core.message(text=color, photo=file)
