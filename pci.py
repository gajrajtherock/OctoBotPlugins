
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import requests
from pprint import pprint
import core
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
PCI_IDS_URL = "https://raw.githubusercontent.com/pciutils/pciids/master/pci.ids"


def get_pci_ids():
    content = requests.get(PCI_IDS_URL).text
    ids = {}
    cvendor = None
    for item in content.split("\n"):
        if item.startswith("#") or item.startswith("\t\t"):
            continue
        elif item.startswith("\t"):
            item = item[1:].split("  ")
            ids[cvendor]["devices"][item[0]] = item[1]
        else:
            cvendor = item.split("  ")
            if len(cvendor) > 1:
                ids[cvendor[0]] = {"devices": {}, "name": cvendor[1]}
                cvendor = cvendor[0]
    return ids


plugin = core.Plugin()
PCI_IDS = get_pci_ids()

@plugin.command(command="/pci",
                description="Searches information about device by it's vendor and device ids. (first arg: vendor, second arg: device)",
                inline_supported=True,
                hidden=False,
                required_args=2)
def search_pci(bot, update, user, args):
    if args[0] in PCI_IDS:
        vendor = PCI_IDS[args[0]]
        vendor_name = vendor["name"]
        if args[1] in vendor["devices"]:
            device_name = vendor["devices"][args[1]]
        else:
            device_name = "Not found"
        return core.message("Vendor name - %s\nDevice name - %s" % (vendor_name, device_name))
    else:
        return core.message("Can't find vendor ID %s" % args[0], failed=True)

if __name__ == '__main__':
    pprint(get_pci_ids())
