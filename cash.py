
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import logging
import csv
from io import StringIO

import requests
import settings
import core

PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="Currency Converter")
CURR_TEMPLATE = """
%(in)s = %(out)s

<a href="http://free.currencyconverterapi.com/">Powered by Currency convert API</a>
"""
LOGGER = logging.getLogger("/cash")

def convert(in_c, out_c, count):
    rate = requests.get(
        "http://free.currencyconverterapi.com/api/v3/convert?compact=ultra",
        params={
            "q": in_c + "_" +  out_c,
            "apiKey": settings.CURRENCY_CONVERTER_API_KEY
        }
    )
    LOGGER.debug(rate.text)
    rate = rate.json()
    if rate == {}:
        raise NameError("Invalid currency")
    else:
        out = {}
        out["in"] = "<b>%s</b> <i>%s</i>" % (count, in_c.upper())
        out["out"] = "<b>%s</b> <i>%s</i>" % (round(float(count)*float(list(rate.values())[0]), 2), out_c.upper())
        return out

@plugin.command(command=["/cash", "/currency"],
                description="Converts currency",
                inline_supported=True,
                required_args=3,
                hidden=False)
def currency(bot, update, user, args):
    """
    Powered by Yahoo Finance
    Example usage:

    User:
    /cash 100 RUB USD

    OctoBot:
    100 RUB = 1.66 USD

    8/7/2017 10:30pm
    Data from Yahoo Finance
    """
    if len(args) < 3:
        return core.message(text="Not enough arguments! Example:<code>/cash 100 RUB USD</code>",
                              parse_mode="HTML",
                              failed=True)
    else:
        try:
            try:
                float(args[0])
            except ValueError:
                return core.message("%s is not a valid number" % args[0], failed=True)
            else:
                rate = convert(args[1], args[-1], args[0])
        except NameError:
            return core.message('Bad currency name', failed=True)
        else:
            return core.message(CURR_TEMPLATE % rate, parse_mode="HTML", extra_args={"disable_web_page_preview":True})
