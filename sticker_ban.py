
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import os
import json
import settings
import logging
from plugins import utils
LOGGER = logging.getLogger("Sticker Block")
PLUGINVERSION = 2
os.makedirs(os.path.normpath("plugdata/block_stickers"), exist_ok=True)
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
LISTING = '<a href="https://t.me/addstickers/%s">%s</a>'
LOCALE_STR = core.locale.get_locales_dict("admin")

def can_delete(user_id, chat):
    for admin in utils.get_admin_ids(chat.bot, chat.id):
        LOGGER.debug(admin)
        if str(admin.user.id) == str(user_id) and (admin.can_delete_messages or admin.status == "creator") or str(user_id) == str(settings.ADMIN):
            return True
    return False


def check_if_user_is_admin(user_id, chat):
    if chat.type == chat.SUPERGROUP:
        for admin in chat.get_administrators():
            if user_id == admin.user.id or str(user_id) == str(settings.ADMIN): # ;)
                return True
        return False
    else:
        return False


def check_everything(me, user, chat):
    if not check_if_user_is_admin(me.id, chat):
        return -2
    elif not check_if_user_is_admin(user.id, chat):
        return -1
    elif str(user.id) == str(settings.ADMIN): # ;)
        return 0
    else:
        return 0

class AdminMessage(core.message):
    def post_init(self):
        core.message.post_init(self)
        self.reply_to_prev_message = False

# Actions
class NotEnoughRights(Exception): pass
class BanningAdmin(Exception): pass

def init_block(chat_id):
    if not os.path.exists(os.path.normpath("plugdata/block_stickers/%s.json" % chat_id)):
        with open(os.path.normpath("plugdata/block_stickers/%s.json" % chat_id), 'w') as f:
            f.write("[]")

@plugin.command(command="/toggle_pack",
                description="Bans stickerpack",
                inline_supported=True,
                hidden=False)
def ban_pack(bot, update, user, args):
    if update.message.chat.type == "supergroup":
        if can_delete(bot.getMe().id, update.message.chat):
            if can_delete(user.id, update.message.chat):
                init_block(update.message.chat.id)
                with open(os.path.normpath("plugdata/block_stickers/%s.json" % update.message.chat.id), 'r',
                          encoding="utf-8") as f:
                    data = json.load(f)
                target_sticker = False
                if update.message.reply_to_message and update.message.reply_to_message.sticker:
                    target_sticker = update.message.reply_to_message.sticker.set_name
                elif len(args) > 0:
                    target_sticker = args[0]
                if target_sticker:
                    if target_sticker in data:
                        result = "Stickerset %s unbanned in this chat" % target_sticker
                        data.remove(target_sticker)
                    else:
                        result = "Stickerset %s banned in this chat" % target_sticker
                        data.append(target_sticker)
                    with open(os.path.normpath("plugdata/block_stickers/%s.json" % update.message.chat.id), 'w', encoding="utf-8") as f:
                        json.dump(data, f)
                    return core.message(text=result)
                else:
                    return core.message(text="Use this command in reply to stickerpack or stickerpack ID.", failed=True)
    else:
        return core.message(text="This command works only in supergroups", failed=True)

@plugin.command(command="/list_bannedstickers",
                description="Lists banned stickerpacks in this chat",
                inline_supported=True,
                hidden=False)
def list_bannedstickers(bot, update, user, args):
    if update.message.chat.type == "supergroup":
        init_block(update.message.chat.id)
        with open(os.path.normpath("plugdata/block_stickers/%s.json" % update.message.chat.id), 'r',
                  encoding="utf-8") as f:
            data = json.load(f)
        msg = []
        for item in data:
            msg.append(LISTING % (item, item))
        if len(msg) > 0:
            return core.message("Currently banned stickerpacks in this chat:\n-" + "\n-".join(msg), parse_mode="HTML")
        else:
            return "There are no banned stickerpacks in this chat."
    else:
        return core.message(text="This command works only in supergroups", failed=True)

@plugin.update()
def check_stricker(bot, update):
    if update.message:
        chat_id = update.message.chat.id
        if update.message.chat.type == "supergroup" and os.path.exists(os.path.normpath("plugdata/block_stickers/%s.json" % chat_id)) and update.message.sticker:
            with open(os.path.normpath("plugdata/block_stickers/%s.json" % update.message.chat.id), 'r', encoding="utf-8") as f:
                data = json.load(f)
            if update.message.sticker.set_name in data:
                update.message.delete()
