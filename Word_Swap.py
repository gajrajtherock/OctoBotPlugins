
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""
Word swap module
"""
import html
import re
import string
import core

PLUGINVERSION = 2
from difflib import Differ

def appendCodeChanges(s1, s2):
    """
    Adds <b></b> tags to words that are changed
    https://stackoverflow.com/a/10775310
    """
    l1 = s1.split(' ')
    l2 = s2.split(' ')
    dif = list(Differ().compare(l1, l2))
    return " ".join(['<code>'+i[2:]+'</code>' if i[:1] == '+' else i[2:] for i in dif
                                                           if not i[:1] in '-?'])
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="Word Swapper")


@plugin.command(command=["s/", "/s/"],
                description="Swaps word in message",
                inline_supported=True,
                hidden=False)
def wordsw(bot, update, user, args):
    """
    Example usage:
    User A
    Hi

    User B
    [In reply to User A]
    /s/Hi/Bye

    OctoBot
    Hello, User A
    Did you mean:
    Bye
    """
    try:
        msg = update.message
        txt = msg.text
        if msg.reply_to_message is not None:
            if not msg.reply_to_message.from_user.id == bot.getMe().id:
                offset = (1 if txt.startswith("/") else 0)
                groups = [b.replace("\/", "/") for b in re.split(r"(?<!\\)/", txt)]
                find = groups[1+offset]
                ready_for_replacement = False
                for letter in find:
                    if not letter in string.punctuation + " ":
                        ready_for_replacement = True
                if not ready_for_replacement or len(find) < 3:
                    print("not ready for replacement")
                    return
                replacement = groups[2+offset]
                # if len(groups) > 3+offset:
                #     flags = groups[3+offset]
                # else:
                #     flags = ""
                # find_re = re.compile("{}{}".format("(?{})".format(flags.replace("g", "")) if flags.replace("g", "") != "" else "", find))
                # mod_msg = html.escape(find_re.sub(replacement, msg.reply_to_message.text, count=100))
                mod_msg = html.escape(msg.reply_to_message.text.replace(find, replacement))
                if msg.reply_to_message.text.replace(find, replacement) == msg.reply_to_message.text:
                    # Nothing changed.
                    return
                mod_msg = appendCodeChanges(msg.reply_to_message.text, mod_msg)
                text = core.locale.get_localized(core.locale.locale_string("did_you_mean", "word_swap"), update.message.chat.id) % (
                        html.escape(msg.reply_to_message.from_user.first_name),
                        mod_msg
                )
                return core.message(text=text, parse_mode="HTML")
    except IndexError: pass
    except re.error: return core.message(text="Invalid regex!", failed=True)
    except AttributeError: return core.message(text="You should reply to message with text!", failed=True)
