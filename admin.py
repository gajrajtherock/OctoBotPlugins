
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import random
import string
import telegram
import settings
from logging import getLogger
import mongoengine
from plugins.utils import get_admin_ids, can_ban, can_pin
LOCALE_STR = core.locale.get_locales_dict("admin")
LOGGER = getLogger("Admin-Plugin")
PLUGINVERSION = 2


def gen_id():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
# MONGO STUFF


def target_username(target_name):
    if str(target_name) == "None":
        return "User"
    else:
        return target_name

mongoengine.connect(host=settings.MONGO_DB_HOST,
                    port=settings.MONGO_DB_PORT, db=settings.MONGO_DB_NAME)


class UserNotFound(ValueError):
    pass


class User(mongoengine.Document):
    username = mongoengine.StringField(max_length=33)
    telegram_id = mongoengine.StringField(max_length=32)


class Warn(mongoengine.EmbeddedDocument):
    target = mongoengine.ReferenceField(User)
    warn_id = mongoengine.StringField(min_length=8, max_length=8)
    reason = mongoengine.StringField(max_length=280)


class Chat(mongoengine.Document):
    warns = mongoengine.EmbeddedDocumentListField(Warn)
    maxwarns = mongoengine.IntField(min_value=2, max_value=10)
    chat_id = mongoengine.StringField(max_length=32)


def get_chat(chat_id):
    chat = Chat.objects(chat_id=str(chat_id))
    if chat.count() == 0:
        chat = Chat()
        chat.warns = []
        chat.chat_id = str(chat_id)
        chat.maxwarns = 3
        chat.save()
    else:
        chat = chat[0]
    return chat


def get_user(uid=None, username=None):
    if uid is not None:
        user = User.objects(telegram_id=str(uid))
        if user.count() == 0:
            user = User()
            user.telegram_id = str(uid)
            user.username = username
            user.save()
        else:
            user = list(user)[0]
            if username is not None:
                user.username = username
            user.save()
        return user
    if username is not None:
        user = User.objects(username=username)
        if user.count == 0:
            raise UserNotFound("No user with such username was found!")
        else:
            return list(user)[0]
    raise ValueError("No arguments specified!")

# TELEGRAM STUFF


class AdminMessage(core.message):
    def post_init(self):
        core.message.post_init(self)
        self.reply_to_prev_message = False

def is_admin(user_id, chat):
    if chat.type == chat.SUPERGROUP:
        for admin in get_admin_ids(chat.bot, chat.id):
            if user_id == admin.user.id or str(user_id) == str(settings.ADMIN):
                return True
        return False
    else:
        return False


class AdminOnly():
    def __init__(self, f):
        LOGGER.debug("Got admin_only attached to %s", f)
        self.function = f

    def __call__(self, bot, update, user, args):
        def _(x): return core.locale.get_localized(x, update.message.chat.id)
        if update.message.chat.type == "supergroup":
            if can_ban(bot.getMe().id, update.message.chat) == True:
                if can_ban(user.id, update.message.chat) == True:
                    # Defining target
                    target = None
                    if update.message.reply_to_message:
                        target = get_user(username=update.message.reply_to_message.from_user.username,
                                          uid=update.message.reply_to_message.from_user.id)
                        reason = " ".join(args)
                    elif len(args) > 0:
                        if args[0].isdigit():
                            target = get_user(uid=args[0])
                            if len(args) > 1:
                                reason = " ".join(args[1:])
                            else:
                                reason = ""
                        elif args[0].startswith("@"):
                            try:
                                target = get_user(username=args[0].strip("@"))
                            except UserNotFound:
                                return AdminMessage(_(LOCALE_STR["invalid_id"]), failed=True)
                            if len(args) > 1:
                                reason = " ".join(args[1:])
                            else:
                                reason = ""
                    # Executing command
                    if target is not None:
                        if str(target.telegram_id) == str(bot.getMe().id):
                            return AdminMessage(_(LOCALE_STR["banning_me"]))
                        try:
                            return self.function(bot, update.message.chat, target, user, reason)
                        except NotEnoughRights:
                            return AdminMessage(_(LOCALE_STR["not_enough_rights"]))
                        except BanningAdmin:
                            return AdminMessage(_(LOCALE_STR["banning_admin"]))
                    else:
                        return AdminMessage(_(LOCALE_STR["no_target"]))
                else:
                    return AdminMessage(_(LOCALE_STR["insufficient_permissions"]), failed=True)
            else:
                return
        else:
            return AdminMessage(_(LOCALE_STR["not_supergroup"]))

# Actions
class NotEnoughRights(Exception): pass
class BanningAdmin(Exception): pass

def ban_user(bot, chat, target):
    try:
        bot.kickChatMember(chat, target)
    except telegram.error.BadRequest as e:
        if "enough rights to restrict" in str(e):
            raise NotEnoughRights
        elif "is an administrator" in str(e):
            raise BanningAdmin


plugin = core.Plugin()


@plugin.message(regex=".*")
def user_overseer(bot, update):
    get_user(uid=update.message.from_user.id,
             username=update.message.from_user.username)
    return


@plugin.command(command="/getwarns",
                description="Shows your warn count",
                inline_supported=False,
                hidden=False)
def warncount(bot, update, user, args):
    def _(x): return core.locale.get_localized(x, update.message.chat.id)
    if update.message.chat.type == "supergroup":
        data = {"user": user.first_name}
        warns = get_chat(update.message.chat.id).warns.filter(
            target=get_user(uid=str(user.id)))
        data["count"] = warns.count()
        data["warns"] = ""
        for warn in warns:
            data["warns"] += "#warn%s - %s" % (warn.warn_id, warn.reason)
        return AdminMessage(text=_(LOCALE_STR["warns"]) % data)
    else:
        return AdminMessage(text=_(LOCALE_STR["not_supergroup"]), failed=True)


@plugin.command(command="/warn",
                description="Warns user. Max length for reason: 280 symbols",
                inline_supported=False)
@AdminOnly
def warn_command(bot, chat, target, admin, reason):
    def _(x): return core.locale.get_localized(x, chat.id)
    data = {
        "target": target.telegram_id,
        "user": target_username(target.username),
        "adminid": admin.id,
        "admin": admin.first_name
    }
    chat_db = get_chat(chat.id)
    uwarn = chat_db.warns.create(target=get_user(
        uid=target.telegram_id), reason=reason, warn_id=gen_id())
    userwarns = chat_db.warns.filter(target=get_user(uid=target.telegram_id))
    LOGGER.debug(userwarns)
    data["current"] = userwarns.count()
    data["reason"] = reason
    data["max"] = chat_db.maxwarns
    data["warn_id"] = "#warn" + str(uwarn.warn_id)
    if data["current"] >= data["max"]:
        userwarns.delete()
        userwarns.save()
        ban_user(bot, chat.id, target.telegram_id)
        return AdminMessage(_(LOCALE_STR["reached_maximum_warns"]) % data, parse_mode="HTML")
    else:
        return AdminMessage(_(LOCALE_STR["was_warned"]) % data, parse_mode="HTML")


@plugin.command(command="/unwarn",
                description="Removes warn from user.",
                inline_supported=False)
@AdminOnly
def unwarn_command(bot, chat, target, admin, reason):
    def _(x): return core.locale.get_localized(x, chat.id)
    data = {
        "target": target.telegram_id,
        "user": target_username(target.username),
        "adminid": admin.id,
        "admin": admin.first_name
    }
    chat_db = get_chat(chat.id)
    userwarns = chat_db.warns.filter(target=get_user(uid=target.telegram_id))
    LOGGER.debug(userwarns)
    if userwarns.count() == 0:
        return AdminMessage(_(LOCALE_STR["cant_unwarn"]) % data, parse_mode="HTML", failed=True)
    else:
        LOGGER.debug("Entries deleted: %s", userwarns.filter(
            warn_id=userwarns[0].warn_id).delete())
        chat_db.save()
        userwarns = chat_db.warns.filter(
            target=get_user(uid=target.telegram_id))
        data["current"] = userwarns.count()
        data["max"] = chat_db.maxwarns
        return AdminMessage(_(LOCALE_STR["warn_removed"]) % data, parse_mode="HTML")


@plugin.command(command="/kick",
                description="Kicks user from chat",
                inline_supported=False)
@AdminOnly
def kick_command(bot, chat, target, admin, reason):
    def _(x): return core.locale.get_localized(x, chat.id)
    data = {
        "target": target.telegram_id,
        "user": target_username(target.username),
        "adminid": admin.id,
        "admin": admin.first_name
    }
    chat_db = get_chat(chat.id)
    userwarns = chat_db.warns.filter(target=get_user(uid=target.telegram_id))
    userwarns.delete()
    chat_db.save()
    ban_user(bot, chat.id, target.telegram_id)
    bot.unbanChatMember(chat.id, target.telegram_id)
    return AdminMessage(_(LOCALE_STR["was_kicked"]) % data, parse_mode="HTML")


@plugin.command(command="/ban",
                description="Bans user from chat",
                inline_supported=False)
@AdminOnly
def ban_command(bot, chat, target, admin, reason):
    def _(x): return core.locale.get_localized(x, chat.id)
    data = {
        "target": target.telegram_id,
        "user": target_username(target.username),
        "adminid": admin.id,
        "admin": admin.first_name
    }
    chat_db = get_chat(chat.id)
    userwarns = chat_db.warns.filter(target=get_user(uid=target.telegram_id))
    userwarns.delete()
    chat_db.save()
    ban_user(bot, chat.id, target.telegram_id)
    return AdminMessage(_(LOCALE_STR["was_banned"]) % data, parse_mode="HTML")

def pin_message(bot, update, user, disable_notifications):
    def _(x): return core.locale.get_localized(x, chat.id)
    chat = update.effective_chat
    if chat.type == 'supergroup' and can_pin(bot.getMe().id, chat):
        if can_pin(user.id, chat):
            if update.message.reply_to_message is not None:
                try:
                    bot.unpin_chat_message(chat.id)
                except telegram.error.BadRequest as e:
                    if "chat_not_modified" in str(e).lower():
                        pass
                    else:
                        raise e
                bot.pin_chat_message(chat.id, update.message.reply_to_message.message_id, disable_notification=disable_notifications)
            else:
                return AdminMessage(_(LOCALE_STR["no_message_to_pin"]), failed=True)
        else:
            return AdminMessage(_(LOCALE_STR["not_enough_rights_of_admin"]), failed=True)

@plugin.command(command=["/pin", "/silent_pin"],
                description="Pins message in chat silently",
                inline_supported=False
                )
def pin_command(bot, update, user, args):
    return pin_message(bot, update, user, True)

@plugin.command(command=["/pin_loud"],
                description="Pins message in chat with notification",
                inline_supported=False
                )
def pin_loud_command(bot, update, user, args):
    return pin_message(bot, update, user, False)
