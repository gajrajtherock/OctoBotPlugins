
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from pprint import pformat
from io import BytesIO

import core
import settings

PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()


@plugin.command(command="//msgdump",
                hidden=True)
def dump(bot, update, user, args):
    text = pformat(update.message.to_dict())
    text_obj = BytesIO()
    text_obj.name = "%s.txt" % update.update_id
    text_obj.write(bytes(text, "utf-8"))
    text_obj.seek(0)
    return core.message(file=text_obj)


@plugin.command(command="//delmsg",
                inline_supported=False,
                hidden=True)
def msgdel(bot, update, user, args):
    if user.id == settings.ADMIN:
        update.message.reply_to_message.delete()


@plugin.command(command="//exec",
                hidden=True)
def docode(bot, update, user, args):
    if user.id == settings.ADMIN:
        return core.message(eval(" ".join(args)))


@plugin.command(command="//plugins", hidden=True)
def coreplug_list(bot, *_):
    message = []
    for plugin in bot.modloader.plugins:
        txt = ''
        if plugin.state == core.constants.OK:
            txt += "✅"
        else:
            txt += "⛔"
        txt += plugin.name
        message.append(txt)
    message = sorted(message)
    message.reverse()
    text = "\n".join(message)
    text_obj = BytesIO()
    text_obj.name = "%s.txt" % "plugins"
    text_obj.write(bytes(text, "utf-8"))
    text_obj.seek(0)
    return core.message(file=text_obj)
