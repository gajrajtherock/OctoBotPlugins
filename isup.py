
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import requests
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
@plugin.command(command="/isup",
                description="Checks if site is up or not",
                inline_supported=True,
                hidden=False)
def isup(bot, update, user, args):
    if args:
        try:
            r = requests.get("http://downforeveryoneorjustme.com/" + args[0])
        except (UnicodeDecodeError, UnicodeDecodeError):
            return core.message(text="There is some problems with isup.me. I cant provide you info on this site, sorry.", failed=True)
        if r.ok:
            r = r.text
            if "looks down" in r:
                return core.message(text="It's not just you! %s looks down from here. " % args[0])
            elif "It's just you" in r:
                return core.message(text="It's just you. %s is up. " % args[0])
            elif "interwho" in r:
                return core.message(text="Huh? %s doesn't look like a site on the interwho. " % args[0])
        else:
            return core.message(text="There is some problems with isup.me. I cant provide you info on this site, sorry.", failed=True)
