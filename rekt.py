
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import random
import core
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
REKTLIST = """
Really Rekt
REKTangle
SHREKT
REKT-it Ralph
Total REKTall
The Lord of the REKT
The Usual SusREKTs
North by NorthREKT
REKT to the Future
Once Upon a Time in the REKT
Full mast erektion
Rektum
Resurrekt
CorRekt
Indirekt
Tyrannosaurus Rekt
Cash4Rekt.com
Grapes of Rekt
Ship Rekt
Rekt markes the spot
Caught rekt handed
The Rekt Side Story
Singin' In The Rekt
Painting The Roses Rekt
Rekt Van Winkle
Parks and Rekt
Lord of the Rekts: The Reking of the King
Star Trekt
The Rekt Prince of Bel-Air
A Game of Rekt
Rektflix
Rekt it like it's hot
RektBox 360
The Rekt-men
School Of Rekt
I am Fire, I am Rekt
Rekt and Roll
Professor Rekt
Catcher in the Rekt
Rekt-22
Harry Potter: The Half-Rekt Prince
Great Rektspectations
Paper Scissors Rekt
RektCraft
Grand Rekt Auto V
Call of Rekt: Modern Reking 2
Legend Of Zelda: Ocarina of Rekt
Rekt It Ralph
Left 4 Rekt
www.rekkit.com
Pokemon: Fire Rekt
The Shawshank Rektemption
The Rektfather
The Rekt Knight
Fiddler on the Rekt
The Rekt Files
The Good, the Bad, and The Rekt
Forrekt Gump
The Silence of the Rekts
The Green Rekt
Gladirekt
Spirekted Away
Terminator 2: Rektment Day
The Rekt Knight Rises
The Rekt King
REKT-E
Citizen Rekt
Requiem for a Rekt
REKT TO REKT ass to ass
Star Wars: Episode VI - Return of the Rekt
Braverekt
Batrekt Begins
2001: A Rekt Odyssey
The Wolf of Rekt Street
Rekt's Labyrinth
12 Years a Rekt
Gravirekt
Finding Rekt
The Arekters
There Will Be Rekt
Christopher Rektellston
Hachi: A Rekt Tale
The Rekt Ultimatum
Shrekt
Rektal Exam
Rektium for a Dream
Erektile Dysfunction
""".split("\n")
REKT_VARIANTS = [
    "☐ not rekt ☐ rekt ☑️ {}",
    "☑️ not rekt ☐ rekt",
    "☐ not rekt ☑️ rekt"]
plugin = core.Plugin(name="Rekt")


@plugin.command(command="/rekt",
                description="rekt",
                inline_supported=True,
                hidden=False)
def rekt(bot, update, user, args):
    return core.message(text=random.choice(REKT_VARIANTS).format(random.choice(REKTLIST)))
