
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import requests
import html
import logging
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
LOGGER = logging.getLogger("DDG")
LOCALE_STR = core.locale.get_locales_dict("generic_responses")
BASE = {"Icon": "", "AbstractText": "", "Results": [], "RelatedTopics": []}


def format_ddg(raw_response):
    ddg_resp = []
    response = dict(BASE)
    response.update(raw_response)
    if response["Icon"] != "":
        ddg_resp.append(f'<a href="{response["Icon"]}">‌‌\u001a</a>')
    if response["AbstractText"] != "":
        ddg_resp.append(html.escape(response["AbstractText"]))
    for result in response["Results"]:
        ddg_resp.append(
            f'''<a href="{result["FirstURL"]}">{html.escape(result["Text"])}</a>''')
    if len(response["RelatedTopics"]) > 0:
        for topic in response["RelatedTopics"]:
            if "Result" in topic:
                ddg_resp.append(topic["Result"])
    ddg_resp.append("""🦆Powered by DuckDuckGo""")
    LOGGER.debug("\n".join(ddg_resp))
    return "\n".join(ddg_resp)


@plugin.command(command=["/duckduckgo", "/ddg", "/search"],
                description="Gives Instant Answers from DuckDuckGo",
                inline_supported=True,
                hidden=False,
                required_args=1)
def ddg(bot, update, user, args):
    def _(x): return core.locale.get_localized(x, update.message.chat.id)
    if " ".join(args).startswith("!"):
        return core.message(text=_(LOCALE_STR["not_found"]))
    r = requests.get("https://api.duckduckgo.com/", params={
        "t": "Telegram-t.me/aigis_bot",
        "q": " ".join(args),
        "format": "json"
    }).json()
    if r["Type"] != "":
        return core.message(text=format_ddg(r), parse_mode='HTML')
    else:
        return core.message(text=_(LOCALE_STR["not_found"]))
