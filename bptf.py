
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import requests
import settings
import textwrap
import core
import json
import re
import logging
from bs4 import BeautifulSoup
from urllib.parse import urljoin, quote
PLUGINVERSION = 2
LOGGER = logging.getLogger("backpacktf")
if settings.SENTRY_ENV != "test":
    PRICES = requests.get("https://backpack.tf/api/IGetPrices/v4",
                          params={
                              "key": settings.BACKPACK_TF_TOKEN
                          }).json()
    LOGGER.debug(PRICES)
    PRICES = PRICES["response"]["items"]
else:
    with open("plugdata/bptf.json", "r") as f:
        PRICES = json.load(f)["response"]["items"]


def parse_unusuals():
    unusuals_s = BeautifulSoup(requests.get(
        "https://backpack.tf/developer/particles").text, "html.parser")
    unusuals = {}
    for unusual in unusuals_s.find_all("td"):
        if unusual.find("small"):
            code = unusual.find("small").text.replace(
                " ", "").replace("\n", "")[1:]
            unusuals[code] = re.sub(
                " +", " ", unusual.text.replace("\n", "")).replace("#" + code, "").strip()
    return unusuals


UNUSUALS = parse_unusuals()
LOGGER.debug(UNUSUALS)
QUALITIES_LIST = {
    "Normal": '0',
    "Genuine": '1',
    "Vintage": '3',
    "Unusual": '5',
    "Unique": '6',
    "Community": '7',
    "Valve": '8',
    "Self-Made": '9',
    "Strange": '11',
    "Haunted": '13',
    "Collector's": '14',
    "Decorated Weapon": "15"
}
QUALITIES_LIST = {v: k for k, v in QUALITIES_LIST.items()}


def format_price(price):
    return textwrap.indent(" <i>%(type)s</i> - <b>%(value)s %(currency)s</b>" % price, " -") + "\n"


def do_search(query):
    query = query.lower()
    LOGGER.debug(query)
    message = ""
    for item_name, item in PRICES.items():
        if query in item_name.lower():
            message += "<b>" + item_name + "</b>\n"
            item = item["prices"]
            for quality, price in item.items():
                message += QUALITIES_LIST[quality] + "\n"
                for itype, subtype_d in price.items():
                    message += "-" + itype + "\n"
                    for name, prices in subtype_d.items():
                        if isinstance(prices, dict):
                            unusuals = []
                            LOGGER.debug(list(prices.items())[:10])
                            for effect, price in list(prices.items())[:10]:
                                price["type"] = UNUSUALS[effect]
                                unusuals.append(format_price(price))
                            unusuals.sort()
                            url = urljoin("https://backpack.tf/unusual/", quote(item_name))
                            unusuals.append('<a href="%s">Other unusuals (%s)</a>\n' % (url, len(prices)))
                            message += "".join(unusuals)

                        else:
                            price = prices[0]
                            price["type"] = name
                            message += format_price(price)
            message += '<a href="%s">This item on backpack.tf</a>' % urljoin(
                "https://backpack.tf/overview/", quote(item_name))
            LOGGER.debug(message)
            return message


# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin("Backpack.TF prices")


@plugin.command(command="/tf_price",
                description="Search for price in backpack.tf",
                inline_supported=True,
                hidden=False,
                required_args=1)
def backpack_tf_search(bot, update, user, args):
    search_res = do_search(" ".join(args))
    if search_res is not None:
        return core.message(search_res, parse_mode="HTML")
    else:
        return core.message("Not found!", failed=True)
