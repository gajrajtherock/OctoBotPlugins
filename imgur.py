
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from telegram_ui import catalog_provider
from requests import get
import bs4
import html
import core
import re
import logging
import settings

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


message = """
<b>%(title)s</b>
<a href="%(link)s">Imgur</a>
"""
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="Imgur")
HEADERS = {"User-Agent": "OctoBot/1.0"}
LOGGER = logging.getLogger("Imgur")


def imgur_search(term, number, count=1):
    import requests

    url = "https://api.imgur.com/3/gallery/search/{{sort}}/{{window}}/{{page}}"

    querystring = {"q":term}

    headers = {'Authorization': 'Client-ID ' + settings.IMGUR_CID}

    pics = requests.request("GET", url, headers=headers, params=querystring).json()

    if not pics == []:
        resp = []
        LOGGER.debug(pics)
        for pic in pics["data"]:
            LOGGER.debug(pic)
            pic["title"] = html.escape(pic["title"][:1024])
            pic["link"] = html.escape(pic["link"])
            if "type" in pic and pic["type"] == "image/gif":
                key = catalog_provider.CatalogKey(text=message % pic, 
                                                  image=pic["link"],
                                                  thumbnail=pic["link"],
                                                  is_gif=True)
            elif "images" in pic:
                key = catalog_provider.CatalogKey(text=message % pic, 
                                                  image=pic["images"][0]["link"],
                                                  thumbnail=pic["images"][0]["link"])
            else:
                key = catalog_provider.CatalogKey(text=message % pic, 
                                                  image=pic["link"],
                                                  thumbnail=pic["link"])
            resp.append(key)
        return resp[number:], len(pics["data"])
    else:
        raise IndexError("Not found")

catalog_provider.create_catalog(plugin, imgur_search, ["/imgur"], "Searches images on Imgur")
