
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

from telegram_ui import catalog_provider
from requests import get
import bs4
import core
import logging


PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin(name="Imgur")
HEADERS = {"User-Agent": "OctoBot/1.0"}
LOGGER = logging.getLogger("TechPowerUp")


def tpu_create_search(searchtype):
    def search(term, number, count=1):
        url = "https://www.techpowerup.com/%s/" % searchtype

        querystring = {"ajaxsrch": term}

        procunits = bs4.BeautifulSoup(
            get(url, params=querystring).text, "lxml")
        procunits = procunits.find_all("tr")
        cell_names = procunits[0].find_all("th")
        if not procunits[1:] == []:
            resp = []
            LOGGER.debug(procunits)
            for procunit in procunits[1:]:
                LOGGER.debug(procunit)
                message = []
                for cell_name in cell_names:
                    cell_value = procunit.find_all(
                        "td")[cell_names.index(cell_name)]
                    message.append(("<b>%s</b>: <i>%s</i>" % (
                        cell_name.text, cell_value.text)).replace("\n", ""))
                message.append('<a href="https://techpowerup.com%s">More info on TechPowerUp</a>' % procunit.find("a")["href"])
                key = catalog_provider.CatalogKey(text="\n".join(message))
                resp.append(key)
            LOGGER.debug(resp)
            return resp[number - 1:], len(procunits[1:])
        else:
            raise IndexError("Not found")
    return search


catalog_provider.create_catalog(
    plugin, tpu_create_search("cpudb"), ["/cpu"], "Searches for CPU on TechPowerUp")

catalog_provider.create_catalog(
    plugin, tpu_create_search("gpudb"), ["/gpu"], "Searches for GPU on TechPowerUp")
