
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import logging
import requests
import html
from bs4 import BeautifulSoup
from telegram_ui import catalog_provider
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
LOGGER = logging.getLogger("GSMArena")
r = requests.get("https://www.gsmarena.com/quicksearch-1.jpg").json()
PHONES = r[1]
MANUFACTURERS = r[0].items()
LOGGER.debug("Loaded " + str(len(PHONES)) + " mobile devices")

def remove_occ(a, b):
    res = []
    for item in a:
        if not item in b:
            res.append(item)
    return res

def get_definition(query, number, count=1):
    phones = []
    phones_l = []
    oem_code = None
    for code, oem in MANUFACTURERS:
        if not oem == "":
            if set(oem.lower().split(" ")) & set(query.lower().split(" ")):
                query = " ".join(remove_occ(query.lower().split(" "), oem.lower().split(" ")))
                oem_code = code
                LOGGER.debug("OEM is %s code is %s", oem, code)
                LOGGER.debug("New query is %s", query)
                break
    for phone in PHONES:
        if oem_code is None or int(phone[0]) == int(oem_code):
            phone_data = {}
            if query.lower() in phone[2].lower():
                LOGGER.debug("Found %s", phone[2])
                phone_data_html = BeautifulSoup(requests.get(
                    "https://m.gsmarena.com/please_give_us_api-%s.php" % phone[1]).text, "html.parser")
                ttls = phone_data_html.find_all("tbody")
                phone_data["_pic"] = phone_data_html.find(class_="specs-cp-pic-rating").find("img")["src"]
                phone_data["_name"] = phone_data_html.find("h1").text
                phone_data["_url"] = "https://m.gsmarena.com/please_give_us_api-%s.php" % phone[1]
                for ttl in ttls:
                    ttl_type = ttl.find("th").text
                    phone_data[ttl_type] = {}
                    for ttl in ttl.find_all("tr")[1:]:
                        LOGGER.debug("Reading tr %s", ttl)
                        if ttl.find(class_="ttl") is not None and ttl.find(class_="nfo") is not None:
                            phone_data[ttl_type][ttl.find(class_="ttl").text] = ttl.find(class_="nfo").text
                phones.append(phone_data)
    for phone in phones:
        phone_s = phone["_name"] + "\n"
        for category_n, category in phone.items():
            if not category_n.startswith("_"):
                phone_s += "<i>%s</i>\n" % html.escape(category_n)
                for ttl, nfo in category.items():
                    if ttl != "\xa0":
                        phone_s += "- <b>%s:</b> %s\n" % (html.escape(ttl), html.escape(nfo))
                    else:
                        phone_s += "- %s\n" % html.escape(nfo)
        phone_s += '<a href="%s">This device on GSMArena</a>' % phone["_url"]
        phones_l.append(catalog_provider.CatalogKey(text=phone_s.replace("\xa0", ""), 
                                                    image=phone["_pic"]))
    if len(phones_l) > 0:
        LOGGER.debug(repr(phones_l[0].text))
        return phones_l[number-count:], len(phones_l)
    else:
        raise IndexError("Not found")

catalog_provider.create_catalog(plugin, get_definition, ["/ga", "/gsmarena"], "Searches for device on GSMArena")
