import telegram

class BaseRule():
    name = "UNNAMED"
    description = "Base rule"

    def __init__(self, code):
        self.code = code

    def check_user(self, user: telegram.User):
        return True, None

    def check_message(self, message: telegram.Message):
        return True, None

    def __repr__(self):
        return f"{type(self).__name__}({self.name}-{format(self.code, '#08x')})"

    def __str__(self):
        return f"{self.name}-{format(self.code, '#06x')}"