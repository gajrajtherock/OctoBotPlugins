
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""
Yandex Translation API
"""
import logging
import html
from telegram import Bot, Update
from requests import post

import settings
import core
PLUGINVERSION = 2

LOGGER = logging.getLogger("YTranslate")
YAURL = "https://translate.yandex.net/api/v1.5/tr.json/translate"
plugin = core.Plugin(name="Yandex Translate")

YTL_TEMPLATE = """
<i>From %(from)s to %(to)s:</i>
<code>%(translation)s</code>
<b>Translated using </b><a href="translate.yandex.com">Yandex.Translate</a>
"""

def get_langs():
    return post("https://translate.yandex.net/api/v1.5/tr.json/getLangs", params={
        "key":settings.YANDEX_TRANSLATION_TOKEN,
        "ui":"en"
    }).json()

LANGS = get_langs()

@plugin.command(command=["/tl", "/translate"],
                description="Translates message to english. Example: [In Reply To Message] /tl",
                inline_supported=True,
                hidden=False)
def translate(bot: Bot, update: Update, user, args):  # pylint: disable=W0613
    """/tl"""
    if len(args) > 0:
        lang = args[0].lower()
        if len(lang.split("-")) > 1:
            from_, to = lang.split("-")
            if not from_ in LANGS["langs"]:
                return core.message("Unknown language - %s" % from_, failed=True)
            if not to in LANGS["langs"]:
                return core.message("Unknown language - %s" % to, failed=True)
            if not lang in get_langs()["dirs"]:
                return core.message("Yandex Translate API doesnt support this direction", failed=True)
    else:
        lang = "en"
    if len(args) > 1:
        text_to_tl = " ".join(args[1:])
    elif update.message.reply_to_message and not update.message.reply_to_message.from_user == bot.getMe():
        text_to_tl = update.message.reply_to_message.text
    else:
        return
    if not text_to_tl == "":
        yandex = post(YAURL, params={
                      "text": text_to_tl, "lang": lang,
                      "key":settings.YANDEX_TRANSLATION_TOKEN}).json()
        if "lang" in yandex:
            message = YTL_TEMPLATE % {
                "translation":html.escape(yandex["text"][0]),
                "from":LANGS["langs"][yandex["lang"].split("-")[0]],
                "to":LANGS["langs"][yandex["lang"].split("-")[1]]
            }
            return core.message(message, parse_mode="HTML", extra_args={"disable_web_page_preview":True})
        else:
            return core.message(yandex["message"], failed=True)
    else:
        return core.message("No text specified!", failed=True)