
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""Have I been pwned module"""
from html import escape
from telegram import Bot, Update
import requests
import core
headers = {
    'User-Agent': 'OctoBotHIBP/1.0'
}


PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()


@plugin.command(command="/pwned",
                description="Have you been hacked?",
                inline_supported=True,
                hidden=False,
                required_args=1)
def pwned(_: Bot, update: Update, user, args):
    """
    Example usage:
    User: /pwned asdasdsad
    Bot: ✅Got cool news for you! You are NOT pwned!
    """
    account = " ".join(args)
    r = requests.get(
        "https://haveibeenpwned.com/api/v2/breachedaccount/%s" % account)
    if r.status_code == 404:
        return core.message("✅Got cool news for you! You are NOT pwned!", use_aliases=True)
    else:
        pwns = r.json()
        message = "⚠️<b>Oh No!</b> You have been <b>pwned</b>:\n<b>Leaked data:</b><i>"
        pwnedthings = {}
        pwnedsites = {}
        for pwn in pwns:
            pwnedsites.update({pwn["Title"]: pwn["Title"]})
            for data in pwn["DataClasses"]:
                pwnedthings.update({data: data})
        message += escape(", ".join(list(pwnedthings)))
        message += "</i>\n<b>From sites:</b><i>\n" + \
            escape("\n".join(list(pwnedsites))) + "</i>"
        return core.message(message, parse_mode="HTML")
