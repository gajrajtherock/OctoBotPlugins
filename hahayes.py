
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import random
import core
plugin = core.Plugin()

EMOJI_BLOCKS = [
        (0x1F300, 0x1F320),
        (0x1F330, 0x1F335),
        (0x1F337, 0x1F37C),
        (0x1F380, 0x1F393),
        (0x1F3A0, 0x1F3C4),
        (0x1F3C6, 0x1F3CA),
        (0x1F3E0, 0x1F3F0),
        (0x1F400, 0x1F43E),
        (0x1F440, 0x1F440),
        (0x1F442, 0x1F4F7),
        (0x1F4F9, 0x1F4FC),
        (0x1F500, 0x1F53C),
        (0x1F540, 0x1F543),
        (0x1F550, 0x1F567),
        (0x1F5FB, 0x1F5FF),
        (0x1F300, 0x1F32C),
        (0x1F330, 0x1F37D),
        (0x1F380, 0x1F3CE),
        (0x1F3D4, 0x1F3F7),
        (0x1F400, 0x1F4FE),
        (0x1F500, 0x1F54A),
        (0x1F550, 0x1F579),
        (0x1F57B, 0x1F5A3),
        (0x1F5A5, 0x1F5FF),
        (0x1F300, 0x1F579),
        (0x1F57B, 0x1F5A3),
        (0x1F5A5, 0x1F5FF)
]


def emojify(string):
    for i in range(string.count(" ")):
        emoji = ''
        for _ in range(random.randint(1,3)):
            block = random.choice(EMOJI_BLOCKS)
            emoji += chr(random.randint(*block))
        string = string.replace(" ", emoji, 1)
    return string

@plugin.command(command="/emojify",
                description="Makes text ugly. People asked me for it. Don't blame me.",
                inline_supported=False,
                hidden=False)
def cmd(bot, update, user, args):
    if update.message.reply_to_message is not None and update.message.reply_to_message.text is not None and len(update.message.reply_to_message.text) > 0:
        text = update.message.reply_to_message.text
    elif len(args) > 0:
        text = " ".join(args)
    else:
        return core.message(text="What I am even supposed to do? You didn't reply to message, you didn't supply arguments", failed=True)
    if len(text) < 500 or update.message.chat.type == "private":
        return core.message(text=emojify(text))
    else:
        return core.message(text="Nope. You won't spam this chat with wall of text and random emojis.", failed=True)

