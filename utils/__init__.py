import time
import settings
class MWT(object):
    """Memoize With Timeout"""
    _caches = {}
    _timeouts = {}

    def __init__(self,timeout=2):
        self.timeout = timeout

    def collect(self):
        """Clear cache of results which have timed out"""
        for func in self._caches:
            cache = {}
            for key in self._caches[func]:
                if (time.time() - self._caches[func][key][1]) < self._timeouts[func]:
                    cache[key] = self._caches[func][key]
            self._caches[func] = cache

    def __call__(self, f):
        self.cache = self._caches[f] = {}
        self._timeouts[f] = self.timeout

        def func(*args, reset=False, return_age=False, **kwargs):
            kw = sorted(kwargs.items())
            print(kw)
            key = (args, tuple(kw))
            try:
                v = self.cache[key]
                if (time.time() - v[1]) > self.timeout or reset:
                    raise KeyError
            except KeyError:
                v = self.cache[key] = f(*args,**kwargs),time.time()
            if return_age:
                return v[1]
            else:
                return v[0]
        func.func_name = f.__name__

        return func

@MWT(timeout=60*60)
def get_admin_ids(bot, chat_id):
    """Returns a list of admin IDs for a given chat. Results are cached for 1 hour."""
    return [admin for admin in bot.get_chat_administrators(chat_id)]

def is_admin(bot, chat_id, user_id):
    for user in get_admin_ids(bot, chat_id):
        if user.user.id == user_id or str(user_id) == str(settings.ADMIN):
            return True
    return False

def can_ban(user_id, chat):
    for admin in get_admin_ids(chat.bot, chat.id):
        if str(admin.user.id) == str(user_id) and (admin.can_restrict_members or admin.status == "creator") or str(user_id) == str(settings.ADMIN):
            return True
    return False

def can_pin(user_id, chat):
    for admin in get_admin_ids(chat.bot, chat.id):
        if str(admin.user.id) == str(user_id) and (admin.can_pin_messages or admin.status == "creator") or str(user_id) == str(settings.ADMIN):
            return True
    return False
