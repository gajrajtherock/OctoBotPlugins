import core
from plugins import utils
import time
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()

@plugin.command(command="/reset_cache",
                description="Resets this supergroup admin cache",
                inline_supported=True,
                hidden=False)
def reset(bot, update, user, args):
    if update.message.chat.type == "supergroup":
        if time.time() - utils.get_admin_ids(bot, update.message.chat_id, return_age=True) < 30:
            return core.message(text="Current cache is too new, please wait about 30 seconds", failed=True)
        else:
            utils.get_admin_ids(bot, update.message.chat_id, reset=True)
            return core.message(text="Chat cache reset completed!")
    else:
        return core.message(text="This command only works in supergroups")

@plugin.command(command="/cache_check",
                description="Checks rights of bot in this supergroup",
                inline_supported=True,
                hidden=False)
def check(bot, update, user, args):
    if update.message.chat.type == "supergroup":
        msg = []
        msg.append("Can I pin messages? %s" % utils.can_pin(bot.getMe().id, update.message.chat))
        msg.append("Can I ban users? %s" % utils.can_ban(bot.getMe().id, update.message.chat))
        msg.append("Cache age: %s second(s)" % int(time.time() - utils.get_admin_ids(bot, update.message.chat_id, return_age=True)))
        msg.append("Reset cache: /reset_cache")
        return core.message("\n".join(msg))
    else:
        return core.message(text="This command only works in supergroups")