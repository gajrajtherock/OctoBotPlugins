
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import re
import core
import html
import requests
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
PLUGINVERSION = 2
MESSAGE = """
<b>%(name)s</b> by <i>%(author)s</i> (%(author_email)s)
Platform: <b>%(platform)s</b>
Version: <b>%(version)s</b>
License: <b>%(license)s</b>
Summary: <i>%(summary)s</i>
"""
def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext
def escape_definition(definition):
    for key, value in definition.items():
        if isinstance(value, str):
            definition[key] = html.escape(cleanhtml(value))
    return definition

# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
@plugin.command(command="/pypi",
                description="Searches for package in PyPI(the place where pip gets its files)",
                inline_supported=True,
                hidden=False,
                required_args=1)
def pypi_search(bot, update, user, args):
    r = requests.get("https://pypi.python.org/pypi/%s/json" % args[0], headers={"User-Agent":"OctoBot/1.0"})
    if r.ok:
        pypi = r.json()["info"]
        kbd = InlineKeyboardMarkup([
            [
            InlineKeyboardButton("Package on PyPI", url=pypi["package_url"]),
            ],
            [
            InlineKeyboardButton("Package home page", url=pypi["home_page"]),
            ]
        ])
        return core.message(MESSAGE % escape_definition(pypi), parse_mode="HTML", inline_keyboard=kbd)
    else:
        return core.message(text="Cant find %s in pypi" % args[0], failed=True)
