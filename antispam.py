
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import core
import logging
import settings
import time
from plugins.utils import get_admin_ids, is_admin

def can_i_ban(bot, chat_id):
    me = bot.getMe()
    for admin in get_admin_ids(bot, chat_id):
        if admin.user.id == me.id and admin.can_restrict_members:
            return True
    return False

LOGGER = logging.getLogger("Antispam")
RULES = []
try:
    import rules_list
except ModuleNotFoundError:
    LOGGER.error("Ruleset is not available - only basic rules will be enabled")
else:
    RULES += rules_list.RULES
    LOGGER.info("Loaded total of %s rules", len(RULES))
PLUGINVERSION = 2
MESSAGE_FORMAT = "%(mention)s was banned by rule %(rule)s. %(message)s"
MESSAGE_CHANNEL_FORMAT = """
Rule: %(rule)s
Name: %(name)s
UID: %(id)s
"""
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
@plugin.update()
def on_update(bot, update):
    if update.message is None or update.message.chat.type != "supergroup" or not can_i_ban(bot, update.message.chat.id):
        return
    banned_users = []
    if update.message.new_chat_members is not None:
        for rule in RULES:
            for user in update.message.new_chat_members:
                rule_check, message = rule.check_user(user)
                if not rule_check:
                    if not is_admin(bot, update.message.chat_id, user.id):
                        bot.kickChatMember(chat_id=update.message.chat.id, user_id=user.id)
                        banned_users.append({"mention":user.mention_html(), "rule":str(rule), "message": message, "name":user.first_name, "id":user.id})
    user = update.message.from_user
    for rule in RULES:
        rule_check, message = rule.check_message(update.message)
        if not rule_check:
            if not is_admin(bot, update.message.chat_id, user.id):
                bot.kickChatMember(chat_id=update.message.chat.id, user_id=user.id)
                banned_users.append(
                    {"mention": user.mention_html(), "rule": str(rule), "message": message, "name": user.first_name,
                     "id": user.id})

    message = []
    for meta in banned_users:
        if meta["message"] is None:
            meta["message"] = ""
        message.append(MESSAGE_FORMAT % meta)
        if settings.BANLOG_CHANNEL is not None:
            bot.sendMessage(chat_id=settings.BANLOG_CHANNEL, text=MESSAGE_CHANNEL_FORMAT % meta)
    if len(message) > 0:
        message.append("\nBot banned someone by mistake? Please report to %s" % settings.CHAT_LINK)
        update.message.reply_text('\n'.join(message), parse_mode='HTML')

@plugin.command(command="//rtest",
                description="rule test",
                inline_supported=True,
                hidden=False)
def ruletest(bot, update, user, args):
    if str(user.id) == str(settings.ADMIN):
        msg = []
        for rule in RULES:
            rule_check, message = rule.check_message(update.message.reply_to_message)
            msg.append("%s - %s - %s" % (rule, rule_check, message))
        return core.message(text="\n".join(msg))