
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import requests

import core
PLUGINVERSION = 2
# Always name this variable as `plugin`
# If you dont, module loader will fail to load the plugin!
plugin = core.Plugin()
TEMPLATE = """
<b>Community</b>:<code>%s</code>
<b>Store</b>:<code>%s</code>
<b>User API</b>:<code>%s</code>
<i>Team Fortress 2</i>
<b>Game coordinator</b>:<code>%s</code>
<b>Inventory</b>:<code>%s</code>
<i>Dota 2</i>
<b>Game coordinator</b>:<code>%s</code>
<b>Inventory</b>:<code>%s</code>
<i>Counter Strike:Global Offensive</i>
<b>Game coordinator</b>:<code>%s</code>
<b>Inventory</b>:<code>%s</code>
"""


@plugin.command(command="/steamstat",
                description="Is steam down?",
                inline_supported=True,
                hidden=False)
def steamstatus(bot, update, user, args):
    steamstatus = requests.get("https://steamgaug.es/api/v2").json()
    message = core.message(TEMPLATE % (
        steamstatus["SteamCommunity"]["error"],
        steamstatus["SteamStore"]["error"],
        steamstatus["ISteamUser"]["error"],
        steamstatus["ISteamGameCoordinator"]["440"]["error"],
        steamstatus["IEconItems"]["440"]["error"],
        steamstatus["ISteamGameCoordinator"]["570"]["error"],
        steamstatus["IEconItems"]["570"]["error"],
        steamstatus["ISteamGameCoordinator"]["730"]["error"],
        steamstatus["IEconItems"]["730"]["error"],
    ), parse_mode="HTML")
    return message
