
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""
Reddit module
"""
import textwrap
import logging
import html

from telegram import Bot, Update
import praw
import prawcore
from settings import REDDITID, REDDITUA, REDDITSECRET
from urllib.parse import urlparse

import core
LOGGER = logging.getLogger("Reddit")
REDDIT = praw.Reddit(client_id=REDDITID,
                     client_secret=REDDITSECRET,
                     user_agent=REDDITUA
                     )

plugin = core.Plugin("Reddit")


@plugin.command(command="/r/",
                description="Shows top posts from subreddit",
                inline_supported=True,
                hidden=False)
def posts(bot: Bot, update: Update, user, args):  # pylint: disable=W0613
    """
    /r/subreddit
    """
    sub = update.message.text.split("/")[2]
    if (not sub == '') and (sub.isalpha()):
        subreddit = REDDIT.subreddit(display_name=sub)
        return core.message(get_subreddit_summary(subreddit), parse_mode="HTML")

def get_comment_summary(comment):
    summary = (f'A comment on <a href="http://redd.it/{comment.submission.id}">{html.escape(comment.submission.title)}</a> '
               f'by <i>{html.escape(comment.author.name)}</i> '
               f'at <a href="http://reddit.com/r/{comment.subreddit.display_name}">{html.escape(comment.subreddit.display_name)}</a> '
               f'{"(WARNING: NSFW)" if comment.submission.over_18 else ""}\n'
               f'{html.escape(textwrap.shorten(comment.body, 1000))}'
               f'\n⬆️{comment.score}')
    return summary


def get_submission_summary(submission):
    summary = (f'{"(WARNING: NSFW) " if submission.over_18 else ""}'
               f'A submission <b>"{html.escape(submission.title)}"</b> by '
               f'<i>{html.escape(submission.author.name)}</i> '
               f'at <a href="http://reddit.com/r/{submission.subreddit.display_name}">{html.escape(submission.subreddit.display_name)}</a>\n'
               f'{html.escape(textwrap.shorten(submission.selftext, 1000))}'
               f'{html.escape(submission.url)}'
               f'\n⬆️{submission.score} (Upvotes ratio: {submission.upvote_ratio*100}%)')
    return summary


def get_subreddit_summary(subreddit):
    summary = (f'{"NSFW" if subreddit.over18 else ""} subreddit '
                f'<a href="https://reddit.com/r/{subreddit.display_name}">{subreddit.display_name}</a>'
                f'\n{subreddit.subscribers} subscribers\n'
                f'<i>{html.escape(textwrap.shorten(subreddit.description, 1000))}</i>')
    top_posts = "\nHot posts in <b>/r/%s</b>:\n\n" % subreddit
    for post in subreddit.hot(limit=10):
        top_posts += ' • <a href="%s">%s</a>\n' % (
            post.shortlink, post.title)
    summary += top_posts
    return summary



@plugin.message(".*")
def comment_handle(bot: Bot, update: Update):
    if isinstance(update.message.entities, list):
        for entity in update.message.entities:
            if entity.type == "url":
                url = update.message.text[entity.offset:entity.offset + entity.length]
                url_parsed = urlparse(url)
                if url_parsed.netloc == "reddit.com" or url_parsed.netloc == "redd.it":
                    try:
                        comment = REDDIT.comment(url=url)
                    except praw.exceptions.ClientException:
                        try:
                            submission = REDDIT.submission(url=url)
                            summary = get_submission_summary(submission)
                        except praw.exceptions.ClientException:
                            display_name = url.split("/r/")[1].split("/")[0]
                            subreddit = REDDIT.subreddit(display_name=display_name)
                            summary = get_subreddit_summary(subreddit)
                    else:
                        summary = get_comment_summary(comment)
                    finally:
                        return core.message(summary, parse_mode="HTML")
